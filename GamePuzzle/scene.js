class Scene {
    constructor(rows, cols) {
        this.division = 50; //Ціна поділки (px)
        this.rows = rows;
        this.cols = cols;
        this.height = this.division * this.rows;
        this.width = this.division * this.cols;
    }

    render(ctx) {
        ctx.lineWidth = 1;
        //Координатна сітка
        ctx.strokeStyle = "#D8D8D8";
        ctx.beginPath();
        for (let i = 0; i < this.rows; i++) {
            ctx.moveTo(0, i * this.division);
            ctx.lineTo(this.width, i * this.division);
        }
        for (let j = 0; j < this.cols; j++) {
            ctx.moveTo(j * this.division, 0);
            ctx.lineTo(j * this.division, this.height);
        }
        ctx.closePath();
        ctx.stroke();

    }
}